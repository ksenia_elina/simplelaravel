<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Temper;
use Illuminate\Support\Facades\Mail;
class WhaetherController extends Controller
{
    //
    public function index(Request $request)
    {
        date_default_timezone_set('Europe/Moscow');
//curl -H 'X-Gismeteo-Token: 56b30cb255.3443075' 'https://api.gismeteo.net/v2/weather/current/4368/
                // 1. инициализация
        $ch = curl_init();
        $url ="https://gridforecast.com/api/v1/forecast/55.5841;37.6097/".date('YmdHi')."?api_token=y8kZEYGHoyG1dPmp";
        // 2. указываем параметры, включая url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. получаем HTML в качестве результата
        $output = curl_exec($ch);

        // 4. закрываем соединение
        curl_close($ch);
        $obj = json_decode($output); 
         // send email
         //прежде. чем отправлять, нужно настроить свой почтовый ящик для отправки в .env и config\mail
        Mail::to($request->email)->send(new Temper($obj->aptmp));
        return view('wether')->with('temp',$obj->aptmp);
    }
}
