<?php

return [

'driver' => 'smtp',
'host' => env('MAIL_HOST', 'smtp.mail.ru'),
'port' => env('MAIL_PORT', '465'),
'from' => ['address' => 'tut', 'name' => 'ksenia'],
'encryption' => env('MAIL_ENCRYPTION','ssl'),
'username' => env('MAIL_USERNAME', 'tut'),
'password' => env('MAIL_PASSWORD', 'tut'),
'sendmail' => '/usr/sbin/sendmail -bs',

'pretend' => false,

];
